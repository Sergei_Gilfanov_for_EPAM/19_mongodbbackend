package com.epam.javatraining2016.autoreapirshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoClientFactoryBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.remoting.jaxws.SimpleJaxWsServiceExporter;
import com.epam.javatraining2016.autoreapirshop.endpoint.AutoRepairShopServiceEndpoint;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;

@SpringBootApplication
@EnableCaching
public class Server extends CachingConfigurerSupport {

  @Bean
  MongoDbFactory mongoDbFactory() throws Exception {
    return new SimpleMongoDbFactory(new MongoClient(), "AutoRepairService");
  }

  @Bean
  AutoRepairShopServiceEndpoint autoRepairShopServiceEndpoint() {
    return new AutoRepairShopServiceEndpoint();
  }

  @Bean
  SimpleJaxWsServiceExporter simpleJaxWsServiceExporter() {
    SimpleJaxWsServiceExporter retval = new SimpleJaxWsServiceExporter();
    retval.setBaseAddress("http://localhost:8089/");
    return retval;
  }

  public static void main(String[] args) throws InterruptedException {
    SpringApplication application = new SpringApplication(Server.class);
    // Нам не нужен встроенный tomcat, SimpleJaxWsServiceExporter использует собственный http сервер
    application.setWebEnvironment(false);
    application.run(args);
  }

}
