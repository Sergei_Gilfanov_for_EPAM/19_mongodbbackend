package com.epam.javatraining2016.autoreapirshop.api;

import com.epam.javatraining2016.autoreapirshop.protocol.Client;
import com.epam.javatraining2016.autoreapirshop.protocol.Comment;
import com.epam.javatraining2016.autoreapirshop.protocol.Order;
import com.epam.javatraining2016.autoreapirshop.protocol.OrderHistoryRecord;
import com.epam.javatraining2016.autoreapirshop.protocol.OrdersPaged;
import com.epam.javatraining2016.autoreapirshop.protocol.UserSearchResult;

public interface AutoRepairShopService {
  public String getVersion();

  public String createOrder(String clientId, String commentText);

  public Order getOrder(String orderId);

  public Order[] getOrdersForClient(String clientId);

  public OrdersPaged getOrdersForClient(String clientId, String fromOrder, int pageLength,
      String status);

  public String changeOrderStatus(String orderId, String statusName, String commentText);

  public String createClient(String name);

  public Client getClient(String clientId);

  public Client[] getClients();

  public void deleteClient(String clientId);

  public String createComment(String orderId, String commentText);

  public OrderHistoryRecord[] getHistory(String id);

  public boolean loginAvailable(String loginName);

  public String createUser(String login, String fullName, String password);

  public UserSearchResult getUser(String loginName);

}
