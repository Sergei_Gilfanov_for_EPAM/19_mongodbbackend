package com.epam.javatraining2016.autoreapirshop.api.impl;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.autoreapirshop.dao.AutoRepairShopServiceDao;
import com.epam.javatraining2016.autoreapirshop.dao.ClientDao;
import com.epam.javatraining2016.autoreapirshop.dao.ClientRow;
import com.epam.javatraining2016.autoreapirshop.dao.CommentRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderDao;
import com.epam.javatraining2016.autoreapirshop.dao.OrderHistoryRecordCommentDao;
import com.epam.javatraining2016.autoreapirshop.dao.OrderHistoryRecordCommentRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderHistoryRecordRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderHistoryRecordStatusChangeDao;
import com.epam.javatraining2016.autoreapirshop.dao.OrderHistoryRecordStatusChangeRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderRow;
import com.epam.javatraining2016.autoreapirshop.dao.UserDao;
import com.epam.javatraining2016.autoreapirshop.dao.UserRow;
import com.epam.javatraining2016.autoreapirshop.protocol.Client;
import com.epam.javatraining2016.autoreapirshop.protocol.Comment;
import com.epam.javatraining2016.autoreapirshop.protocol.Order;
import com.epam.javatraining2016.autoreapirshop.protocol.OrderHistoryRecord;
import com.epam.javatraining2016.autoreapirshop.protocol.OrderHistoryRecordComment;
import com.epam.javatraining2016.autoreapirshop.protocol.OrderHistoryRecordStatusChange;
import com.epam.javatraining2016.autoreapirshop.protocol.OrdersPaged;
import com.epam.javatraining2016.autoreapirshop.protocol.User;
import com.epam.javatraining2016.autoreapirshop.protocol.UserSearchResult;

@Component(value = "autoRepairShopService")
public class AutoRepairShopServiceImpl implements AutoRepairShopService {
  private static final Logger log = LoggerFactory.getLogger(AutoRepairShopServiceImpl.class);

  @Autowired
  UserDao userDao;

  @Autowired
  ClientDao clientDao;

  @Autowired
  OrderDao orderDao;

  @Autowired
  OrderHistoryRecordCommentDao orderHistoryRecordCommentDao;

  @Autowired
  OrderHistoryRecordStatusChangeDao orderHistoryRecordStatusChangeDao;


  public AutoRepairShopServiceImpl() {
    log.info("AutoRepairShopServiceImpl:default constructor");
  }

  @Override
  public String getVersion() {
    return "sergei_gilfanov@epam.com,2016-06-27";
  }

  @Override
  public String createClient(String name) {
    log.info("createClient");
    ClientRow retval;
    List<OrderRow> orderList = new ArrayList<OrderRow>(0);
    retval = clientDao.create(name, orderList);
    return retval.getId();
  }

  @Override
  @Transactional
  public Client getClient(String clientId) {
    log.info("getClient");
    ClientRow client;
    client = clientDao.getShallow(clientId);

    Client retval = new Client();
    retval.setId(client.getId());
    retval.setClientName(client.getClientName());
    return retval;
  }

  @Override
  @Transactional
  public Client[] getClients() {
    log.info("getClients");

    List<Client> retval;
    List<ClientRow> clients = clientDao.getAll();
    retval = new ArrayList<Client>(clients.size());
    for (ClientRow clientRow : clients) {
      Client client = new Client();
      client.setId(clientRow.getId());
      client.setClientName(clientRow.getClientName());
      retval.add(client);
    }
    return retval.toArray(new Client[0]);
  }

  @Override
  public void deleteClient(String clientId) {
    log.info("deleteClient");
    clientDao.delete(clientId);
  }

  @Override
  @Transactional
  public String createComment(String orderId, String commentText) {
    return orderHistoryRecordCommentDao.addToOrder(orderId, commentText);
  }

  @Override
  @Transactional
  public String createOrder(String clientId, String commentText) {
    log.info("createOrder");
    OrderRow order;
    String retval = orderDao.addToClient(clientId, commentText);
    return retval;
  }

  @Override
  @Transactional
  public Order getOrder(String orderId) {
    OrderRow order = orderDao.findById(orderId);
    if (order == null) {
      return null;
    }
    String status = orderDao.getCurrentStatus(order);

    Order retval = new Order();
    retval.setId(order.getId());
    retval.setStatus(status);
    return retval;
  }

  @Override
  @Transactional
  public String changeOrderStatus(String orderId, String statusName, String commentText) {
    return orderHistoryRecordStatusChangeDao.addToOrder(orderId, statusName, commentText);
  }

  @Override
  @Transactional
  public Order[] getOrdersForClient(String clientId) {
    List<Order> retval;
    List<OrderRow> orders = orderDao.findByClientId(clientId);
    retval = new ArrayList<Order>(orders.size());
    for (OrderRow orderRow : orders) {
      Order order = new Order();
      String orderStatus = orderDao.getCurrentStatus(orderRow);
      order.setId(orderRow.getId());
      order.setStatus(orderStatus);
      retval.add(order);
    }
    return retval.toArray(new Order[0]);
  }

  @Override
  @Transactional
  public OrdersPaged getOrdersForClient(String clientId, String fromOrder, int pageLength,
      String status) {
    OrdersPaged retval = new OrdersPaged();
    retval.setNextPageFrom(null);
    retval.setPrevPageFrom(null);
    List<OrderRow> orders;
    // Читаем на одну строчку больше - если это получилось, то это строчка будет началом следующей
    // страницы
    if (fromOrder.equals("")) {
      orders = clientDao.readOrdersFirstPage(clientId, pageLength + 1, status);
    } else {
      orders = clientDao.readOrdersPage(clientId, fromOrder, pageLength + 1, status);
    }

    if (orders.size() >= pageLength + 1) {
      OrderRow startOfNextPage = orders.remove(orders.size() - 1);
      retval.setNextPageFrom(startOfNextPage.getId());
    }

    if (! fromOrder.equals("") ) { // Для первой страницы предыдущей заведомо нет
      OrderRow startOfPrevPage = clientDao.findPrevPage(clientId, fromOrder, pageLength, status);
      if (startOfPrevPage != null) {
        retval.setPrevPageFrom(startOfPrevPage.getId());
      }
    }

    for (OrderRow orderRow : orders) {
      Order order = new Order();
      order.setId(orderRow.getId());
      order.setStatus(orderDao.getCurrentStatus(orderRow));
      retval.getOrder().add(order);
    }
    return retval;
  }

  @Override
  public OrderHistoryRecord[] getHistory(String orderId) {
    List<OrderHistoryRecordRow> orderHistory = orderDao.getHistory(orderId);
    if (orderHistory == null) {
      return null;
    }
    List<OrderHistoryRecord> retval = new ArrayList<OrderHistoryRecord>(orderHistory.size());
    for (OrderHistoryRecordRow historyRecordRow : orderHistory) {
      OrderHistoryRecord toAdd = null;
      if (historyRecordRow instanceof OrderHistoryRecordStatusChangeRow) {
        OrderHistoryRecordStatusChange toAddStatusChange = new OrderHistoryRecordStatusChange();
        OrderHistoryRecordStatusChangeRow orderHistoryRecordStatusChangeRow =
            (OrderHistoryRecordStatusChangeRow) historyRecordRow;
        toAddStatusChange.setStatus(orderHistoryRecordStatusChangeRow.getOrderStatus());
        Comment comment = new Comment();
        comment.setCommentText(orderHistoryRecordStatusChangeRow.getCommentText());
        toAddStatusChange.setComment(comment);
        toAdd = toAddStatusChange;
      } else {
        OrderHistoryRecordComment toAddComment = new OrderHistoryRecordComment();
        OrderHistoryRecordCommentRow orderHistoryCommentRow =
            (OrderHistoryRecordCommentRow) historyRecordRow;
        Comment comment = new Comment();
        comment.setCommentText(orderHistoryCommentRow.getCommentText());
        toAddComment.setComment(comment);
        toAdd = toAddComment;
      }
      toAdd.setId(historyRecordRow.getId());
      retval.add(toAdd);
    }
    return retval.toArray(new OrderHistoryRecord[0]);
  }

  @Override
  public boolean loginAvailable(String loginName) {
    log.info("login");
    UserRow userRow = null;
    userRow = userDao.findByLogin(loginName);

    return userRow == null;
  }

  @Override
  public String createUser(String login, String fullName, String password) {
    UserRow retval = userDao.create(login, fullName, password);
    return retval.getId();
  }

  @Override
  public UserSearchResult getUser(String loginName) {
    UserRow userRow = null;
    UserSearchResult retval = new UserSearchResult();
    retval.setFound(true);
    userRow = userDao.findByLogin(loginName);

    if (userRow == null) {
      retval.setFound(false);
      retval.setUser(null);
      return retval;
    }

    User user = new User();
    user.setId(userRow.getId());
    user.setLogin(loginName);
    user.setPassword(userRow.getPassword());
    user.setFullName(userRow.getFullName());
    user.setManager(userRow.isManager());
    retval.setUser(user);
    return retval;
  }
}
