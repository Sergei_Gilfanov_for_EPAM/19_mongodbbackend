package com.epam.javatraining2016.autoreapirshop.dao;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="user")
public class UserRow {
  private String id;
  private String login;
  private String fullName;
  private String password;
  private boolean manager;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String name) {
    this.fullName = name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean isManager() {
    return manager;
  }

  public void setManager(boolean manager) {
    this.manager = manager;
  }
  
}
