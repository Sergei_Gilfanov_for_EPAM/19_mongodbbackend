package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Component;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;
@Component
public class UserDao {
  @Autowired
  private MongoOperations mongoOps;

  public UserDao() {
  }

  public UserRow create(String login, String fullName, String password) {
    UserRow retval = new UserRow();
    retval.setLogin(login);
    retval.setFullName(fullName);
    retval.setPassword(password);
    mongoOps.insert(retval);
    return retval;
  }

  public UserRow get(int userId) {
    UserRow retval = mongoOps.findById(userId, UserRow.class);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }

  public UserRow findByLogin(String login) {
    UserRow retval = mongoOps.findOne(query(where("login").is(login)), UserRow.class);
    return retval;
  }
}
