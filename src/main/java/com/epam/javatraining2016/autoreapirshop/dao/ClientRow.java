package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Client")
@CompoundIndexes({
  @CompoundIndex(name = "order_idx", def = "{orderList._id: 1}")
})
public class ClientRow {
  @Id
  @Indexed
  private String id;
  private List<OrderRow> orderList;
  private String clientName;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public List<OrderRow> getOrderList() {
    return orderList;
  }

  public void setOrderList(List<OrderRow> orderList) {
    this.orderList = orderList;
  }

  public String getClientName() {
    return clientName;
  }

  public void setClientName(String clientName) {
    this.clientName = clientName;
  }


}
