package com.epam.javatraining2016.autoreapirshop.dao;

import org.springframework.data.mongodb.core.mapping.Document;

public class OrderHistoryRecordCommentRow extends OrderHistoryRecordRow {
  private String commentText;

  public String getCommentText() {
    return commentText;
  }

  public void setCommentText(String commentText) {
    this.commentText = commentText;
  }
}
