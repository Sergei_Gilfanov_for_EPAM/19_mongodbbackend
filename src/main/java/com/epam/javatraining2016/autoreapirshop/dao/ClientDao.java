package com.epam.javatraining2016.autoreapirshop.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

@Component
public class ClientDao {
  @Autowired
  private MongoOperations mongoOps;
  @Autowired
  private OrderDao orderDao;

  public ClientDao() {}

  public ClientRow create(String name, List<OrderRow> orderList) {
    ClientRow retval = new ClientRow();
    retval.setClientName(name);
    retval.setOrderList(orderList);
    mongoOps.insert(retval);
    return retval;
  }

  public ClientRow getShallow(String clientId) {
    // TODO: сделать Shallow
    ClientRow retval = mongoOps.findById(clientId, ClientRow.class);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }

  public List<ClientRow> getAll() {
    List<ClientRow> retval = mongoOps.findAll(ClientRow.class);
    return retval;
  }

  public void delete(String clientId) {
    ClientRow todelete = new ClientRow();
    todelete.setId(clientId);
    mongoOps.remove(todelete);
  }

  public List<OrderRow> readOrdersFirstPage(String clientId, int pageLength, String status) {
    List<OrderRow> orders = orderDao.findByClientId(clientId);
    Stream<OrderRow> ordersFilteredStream;
    if (!status.equals("")) {
      ordersFilteredStream =
          orders.stream().filter((order) -> orderDao.getCurrentStatus(order).equals(status));
    } else {
      ordersFilteredStream = orders.stream();
    }
    List<OrderRow> ordersFiltered = ordersFilteredStream.collect(Collectors.toList());
    Collections.reverse(ordersFiltered);
    int outputTo = Integer.min(pageLength, ordersFiltered.size());
    return ordersFiltered.subList(0, outputTo);
  }

  public List<OrderRow> readOrdersPage(String clientId, String fromOrder, int pageLength,
      String status) {
    List<OrderRow> orders = orderDao.findByClientId(clientId);
    Stream<OrderRow> ordersFilteredStream;
    if (!status.equals("")) {
      ordersFilteredStream =
          orders.stream().filter((order) -> orderDao.getCurrentStatus(order).equals(status)
              && order.getId().compareTo(fromOrder) <= 0);
    } else {
      ordersFilteredStream =
          orders.stream().filter((order) -> order.getId().compareTo(fromOrder) <= 0);
    }
    List<OrderRow> ordersFiltered = ordersFilteredStream.collect(Collectors.toList());
    Collections.reverse(ordersFiltered);
    int outputTo = Integer.min(pageLength, ordersFiltered.size());
    return ordersFiltered.subList(0, outputTo);
  }

  // Ищем начало предыдущей страницы (более новые заказы)
  public OrderRow findPrevPage(String clientId, String fromOrder, int pageLength, String status) {
    List<OrderRow> orders = orderDao.findByClientId(clientId);
    Stream<OrderRow> ordersAfter =
        orders.stream().filter((order) -> order.getId().compareTo(fromOrder) > 0);
    Stream<OrderRow> ordersAfterFilteredStream;
    if (!status.equals("")) {
      ordersAfterFilteredStream =
          ordersAfter.filter((order) -> orderDao.getCurrentStatus(order).equals(status));
    } else {
      ordersAfterFilteredStream = ordersAfter;
    }
    Stream<OrderRow> ordersAfterFilteredLastPageStream = ordersAfterFilteredStream.limit(pageLength);
    Optional<OrderRow> lastElement = ordersAfterFilteredLastPageStream.reduce((first, second) -> second);
    if (lastElement.isPresent() ) {
      return lastElement.get();
    } else {
      return null;
    }
  }
}
