package com.epam.javatraining2016.autoreapirshop.dao;

import org.springframework.data.mongodb.core.mapping.Document;

public class OrderHistoryRecordStatusChangeRow extends OrderHistoryRecordRow {
  private String orderStatus;
  private String commentText;

  public String getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(String orderStatus) {
    this.orderStatus = orderStatus;
  }

  public String getCommentText() {
    return commentText;
  }

  public void setCommentText(String commentText) {
    this.commentText = commentText;
  }

}
