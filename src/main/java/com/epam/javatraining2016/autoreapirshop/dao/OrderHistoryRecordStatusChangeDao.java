package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.List;
import java.util.NoSuchElementException;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import static org.springframework.data.mongodb.core.query.Criteria.where;

@Component
public class OrderHistoryRecordStatusChangeDao {
  @Autowired
  private MongoOperations mongoOps;

  public OrderHistoryRecordStatusChangeDao() {}

  public String addToOrder(String orderId, String orderStatus, String commentText) {
    OrderHistoryRecordStatusChangeRow orderHistoryRecord = new OrderHistoryRecordStatusChangeRow();
    orderHistoryRecord.setId(new ObjectId().toString());
    orderHistoryRecord.setOrderStatus(orderStatus);
    orderHistoryRecord.setCommentText(commentText);

    Criteria selectorExpression = where("orderList").elemMatch(where("_id").is(orderId));
    Query query = new Query(selectorExpression);

    Update update = new Update().push("orderList.$.orderHistory", orderHistoryRecord);
    mongoOps.updateFirst(query, update, ClientRow.class);
    return orderHistoryRecord.getId();
  }
}
