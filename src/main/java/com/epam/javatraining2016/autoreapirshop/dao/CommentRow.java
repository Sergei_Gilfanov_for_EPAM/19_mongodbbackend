package com.epam.javatraining2016.autoreapirshop.dao;

public class CommentRow {
  private String id;
  private String commentText;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCommentText() {
    return commentText;
  }

  public void setCommentText(String commentText) {
    this.commentText = commentText;
  }

}
