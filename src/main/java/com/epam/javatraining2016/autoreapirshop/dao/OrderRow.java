package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

public class OrderRow {
  private String id;
  private List<InvoiceRow> invoiceList;
  private List<OrderHistoryRecordRow> orderHistory;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public List<InvoiceRow> getInvoiceList() {
    return invoiceList;
  }

  public void setInvoiceList(List<InvoiceRow> invoiceList) {
    this.invoiceList = invoiceList;
  }

  public List<OrderHistoryRecordRow> getOrderHistory() {
    return orderHistory;
  }

  public void setOrderHistory(List<OrderHistoryRecordRow> orderHistory) {
    this.orderHistory = orderHistory;
  }

}
