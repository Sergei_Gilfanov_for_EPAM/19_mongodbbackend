package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Field;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.epam.javatraining2016.autoreapirshop.protocol.OrderHistoryRecord;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Component
public class OrderDao {
  @Autowired
  private MongoOperations mongoOps;

  public OrderDao() {}

  public String addToClient(String clientId, String commentText) {
    OrderRow orderRow = new OrderRow();
    orderRow.setId(new ObjectId().toString());
    orderRow.setInvoiceList(new ArrayList<InvoiceRow>(0));

    OrderHistoryRecordStatusChangeRow historyRecord = new OrderHistoryRecordStatusChangeRow();
    historyRecord.setId(new ObjectId().toString());
    historyRecord.setOrderStatus("new");
    historyRecord.setCommentText(commentText);

    List<OrderHistoryRecordRow> orderHistory = new ArrayList<OrderHistoryRecordRow>(1);
    orderHistory.add(historyRecord);
    orderRow.setOrderHistory(orderHistory);

    Query query = new Query(where("_id").is(clientId));
    Update update = new Update().push("orderList", orderRow);
    mongoOps.updateFirst(query, update, ClientRow.class);
    return orderRow.getId();
  }

  public OrderRow findById(String orderId) {
    Criteria selectorExpression = where("orderList").elemMatch(where("_id").is(orderId));
    Query query = new Query(selectorExpression);
    query.fields().include("orderList.$:1");
    List<ClientRow> clients = mongoOps.find(query, ClientRow.class);
    // Если мы все делали правильно, то при удачном поиске clients всегда содержит один элемент
    // (клиент, которому принадлежит заказ)
    // А его поле orderList - содержит один элемент - найденный заказ
    if (clients.isEmpty()) {
      return null;
    }
    return clients.get(0).getOrderList().get(0);
  }

  public String getCurrentStatus(OrderRow order) {
    String retval = "";
    for (OrderHistoryRecordRow historyRecord : order.getOrderHistory()) {
      if (historyRecord instanceof OrderHistoryRecordStatusChangeRow) {
        retval = ((OrderHistoryRecordStatusChangeRow) historyRecord).getOrderStatus();
      }
    }
    return retval;
  }

  public List<OrderRow> findByClientId(String clientId) {
    Criteria selectorExpression = where("_id").is(clientId);
    Query query = new Query(selectorExpression);
    List<ClientRow> clients = mongoOps.find(query, ClientRow.class);
    // Если мы все делали правильно, то при удачном поиске clients всегда содержит один элемент
    // (клиент, которому принадлежит заказ)
    if (clients.isEmpty()) {
      return null;
    }
    return clients.get(0).getOrderList();
  }

  public List<OrderHistoryRecordRow> getHistory(String orderId) {
    Criteria selectorExpression = where("orderList").elemMatch(where("_id").is(orderId));
    Query query = new Query(selectorExpression);
    query.fields().include("orderList.$:1");
    List<ClientRow> clients = mongoOps.find(query, ClientRow.class);
    // Если мы все делали правильно, то при удачном поиске clients всегда содержит один элемент
    // (клиент, которому принадлежит заказ)
    // А его поле orderList - содержит один элемент - найденный заказ
    if (clients.isEmpty()) {
      return null;
    }
    return clients.get(0).getOrderList().get(0).getOrderHistory();
  }
}
