package com.epam.javatraining2016.autoreapirshop.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

@Component
public class AutoRepairShopServiceDao implements AutoCloseable {
  // private static final Logger log = LoggerFactory.getLogger(AutoRepairShopServiceDao.class);

  public AutoRepairShopServiceDao() {
  }

  @Override
  public void close() {
  }

  public void commit() {
  }

  @Autowired
  public OrderDao order;

  @Autowired
  public ClientDao client;

  @Autowired
  public UserDao user;
}
