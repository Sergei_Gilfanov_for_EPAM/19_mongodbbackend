package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.List;
import java.util.NoSuchElementException;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import static org.springframework.data.mongodb.core.query.Criteria.where;

@Component
public class OrderHistoryRecordCommentDao {
  @Autowired
  private MongoOperations mongoOps;

  public OrderHistoryRecordCommentDao() {}

  public String addToOrder(String orderId, String commentText) {
    OrderHistoryRecordCommentRow orderHistoryRecord = new OrderHistoryRecordCommentRow();
    orderHistoryRecord.setId(new ObjectId().toString());
    orderHistoryRecord.setCommentText(commentText);

    // Селектор клиента, которому принадлежит заказ
    Criteria selectorExpression = where("orderList").elemMatch(where("_id").is(orderId));
    Query query = new Query(selectorExpression);

    Update update = new Update().push("orderList.$.orderHistory", orderHistoryRecord);
    mongoOps.updateFirst(query, update, ClientRow.class);
    return orderHistoryRecord.getId();
  }
}
