-- Создаем первого, специального пользователя-анонима.
	-- Пустой список заказов для клиента
	insert into OrderList(id) values (nextval('orderlist_id_seq'));
insert into Client(deleted, client_name, order_list_id) values(false, 'Неизвестный клиент', currval('orderlist_id_seq'));

-- Пароль - "a"
insert into Users(login, full_name, password, manager) values( 'user1', 'Первый пользователь', '$2a$15$xOFXPJUGFX8vZtRmgsxWOu9FsfyGecMSB7uveagsOWowEnfefImDm', true);
-- Пароль - "b"
insert into Users(login, full_name, password, manager) values( 'user2', 'Второй пользователь', '$2a$15$Z1vYp8Sp9pUJWwviIEviOe7y05mr5m.qB8YQZssiBhwj2p57uhK5q', false);
